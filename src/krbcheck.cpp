/*
 * $Id: main.cpp 79 2006-01-19 18:19:07Z noahm $
 */

#include "kredentials.h"
#include "krb5_wrap.h"
#include <iostream>



/**
 Debuggable main without detaching for testing the krb5_functions.
*/
int main (int , char)
{
    Krb5::TicketManager tix (TRUE);
    for (int i = 0; i < 3; i++) {
        tix.initKerberos();
        tix.hasTicket();
        tix.renewTicket();
        tix.runAklog();
    }
    tix.destroyTickets();
    tix.runAklog();
    tix.hasTicket();
    tix.renewTicket();
    tix.initKerberos();
    std::cout << "Supply password: ";
    tix.getCredentials (tix.readPass() );
    for (int i = 0; i < 3; i++) {
        tix.initKerberos();
        tix.hasTicket();
        tix.renewTicket();
        tix.runAklog();
    }
    return 0;
}

