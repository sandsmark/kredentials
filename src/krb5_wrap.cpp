/*
 * Copyright 2006 by Martin Ginkel, MPI-Magdeburg <mginkel@mpi-magdeburg.mpg.de>
 * and Copyright 2004 by the Massachusetts Institute of Technology.
 * All Rights Reserved.
 *
 * Permission to use, copy, modify, and distribute this software and
 * its documentation for any purpose and without fee is hereby
 * granted, provided that the above copyright notice appear in all
 * copies and that both that copyright notice and this permission
 * notice appear in supporting documentation, and that the name of
 * M.I.T. not be used in advertising or publicity pertaining to
 * distribution of the software without specific, written prior
 * permission.  Furthermore if you modify this software you must label
 * your software as modified software and not distribute it in such a
 * fashion that it might be confused with the original
 * M.I.T. software.  M.I.T. makes no representations about the
 * suitability of this software for any purpose.  It is provided "as
 * is" without express or implied warranty.
 *
 * $Id: krb5_wrap.cpp 119 2007-03-13 14:38:00Z noahm $
 */

#include <kdebug.h>
#include <krb5_wrap.h>
#include <iostream>
#include <termios.h>
#include <pwd.h>

namespace Krb5 {
using namespace std;

int Base::error() const {
    return kerror;
};

Context::Context() : Base() {
    kerror = krb5_init_context (&_ctx);
    if (kerror) {
        kDebug() << "context() Kerberos returned " << kerror;
        return;
    }
    /*
      throw(std::string("Kerberos returned ")+kerror);
    */
}

Context::~Context() {
    krb5_free_context (_ctx);
}

krb5_context Context::operator() () const {
    return _ctx;
}

void Context::reinit() {
    krb5_free_context (_ctx);
    kerror = krb5_init_context (&_ctx);
    if (kerror) {
        kDebug() << "context::reinit() Kerberos returned " << kerror;
        return;
    }
}


CredentialCache::CredentialCache (Context& _ctx) : Base(), ctx (_ctx),
        _principal (NULL),
        _creds (NULL) {
    kerror = krb5_cc_default (ctx(), &_cc);
    if (kerror) {
        kDebug() << "ccache() Kerberos returned " << kerror;
        return;
    }
    /*
      throw(std::string("Kerberos returned ")+kerror);

    */
}

CredentialCache::~CredentialCache() {
    if (_principal)
        delete _principal;
    if (_creds)
        delete _creds;
}

krb5_ccache CredentialCache::operator() () const {
    return _cc;
}

Context& CredentialCache::getCtx() const {
    return ctx;
}

CredentialCacheIterator CredentialCache::iterator() {
    return CredentialCacheIterator (*this);
}

const std::string& CredentialCache::name() {
    _name = krb5_cc_get_name (ctx(), _cc);
    return _name;
}

Principal* CredentialCache::getPrincipal() {
    if (_principal == NULL) {
        _principal = new Principal (ctx, *this);
        kerror = _principal->error();
        if (kerror) {
            kDebug() << "ccache() Kerberos returned " << kerror;
            delete _principal;
            _principal = NULL;
        }
    }
    return _principal;
}

void CredentialCache::setPrincipal (Principal& me) {
    Principal* pme = getPrincipal();
    if (pme) {
        if (krb5_principal_compare (ctx(), (*pme) (), me() ) )
            return;
    }
    kerror = krb5_cc_initialize (ctx(), _cc, me() );
}

Credentials& CredentialCache::renew_creds() {
    if (_creds == NULL)
        _creds = new Credentials (ctx);
    _creds->clear();
    Principal* pme = getPrincipal();
    if (pme) {
        kerror = krb5_get_renewed_creds (ctx(), (*_creds) (),
                                         (*pme) (), _cc, NULL);
    }
    return *_creds;
}

void CredentialCache::store (Credentials& creds) {
    Principal* pme = getPrincipal();
    if (pme) {
        kerror = krb5_cc_initialize (ctx(), _cc, (*pme) () );
        if (kerror) return;
        kerror = krb5_cc_store_cred (ctx(), _cc, creds() );
    }
}

CredentialCache& CredentialCache::operator= (const CredentialCache& o) {
    if (_principal) {
        delete _principal;
        _principal = NULL ;
    };
    if (_creds) {
        delete _creds;
        _creds = NULL;
    };
    ctx = o.getCtx();
    kerror = krb5_cc_default (ctx(), &_cc);
    return *this;
}

void CredentialCache::destroy() {
    kerror = krb5_cc_destroy (ctx(), _cc);
    if (kerror) return;
    kerror = krb5_cc_default (ctx(), &_cc);
}

CredentialCacheIterator::CredentialCacheIterator (CredentialCache& _cc) : Base(), cc (_cc), pcreds (NULL) {
    kerror = krb5_cc_start_seq_get (cc.getCtx() (), cc(), &cur);
    if (kerror) {
        kDebug() << "ccIter() Kerberos returned " << kerror;
        return;
    }

}

CredentialCacheIterator::~CredentialCacheIterator() {
    if (pcreds != NULL) {
        delete pcreds;
    }
}

void CredentialCacheIterator::next() {
    if (pcreds != NULL) {
        delete pcreds;
        pcreds = NULL;
    }
    krb5_creds* tmpCreds = new krb5_creds();
    kerror = krb5_cc_next_cred (cc.getCtx() (), cc(), &cur, tmpCreds);
    if (kerror) {
        kDebug() << "ccIter::next() Kerberos returned " << kerror << " : " << this->error();
        delete tmpCreds;
        return;
    }
    pcreds = new Credentials (cc.getCtx(), tmpCreds);
}

const Credentials& CredentialCacheIterator::operator*() {
    if (pcreds == NULL)
        next();
    return *pcreds;
}

CredentialCacheIterator& CredentialCacheIterator::operator++ (int ) {
    if (!kerror) {
        next();
    }
    return *this;
}



Principal::Principal (Context& _ctx, CredentialCache& _cc) : Base(), free (TRUE), ctx (_ctx), _principal (NULL) {
    kerror = krb5_cc_get_principal (ctx(), _cc(), &_principal);
    if (kerror)
        return;
    /*
      throw(std::string("Kerberos returned ")+kerror);
    */
};

Principal::Principal (Context& _ctx, krb5_principal p, bool copy) : Base(), free (copy), ctx (_ctx), _principal (NULL) {
    if (copy)
        kerror = krb5_copy_principal (ctx(), p, &_principal);
    else
        _principal = p;
    if (kerror)
        return;
    /*
      throw(std::string("Kerberos returned ")+kerror);
    */
};

Principal::Principal (Principal& o) : Base(), free (TRUE), ctx (o.getCtx() ), _principal (NULL) {
    if (o() != NULL)
        krb5_copy_principal (ctx(), o(), &_principal);
};

Principal::~Principal() {
    if ( (_principal != NULL) && free)
        krb5_free_principal (ctx(), _principal);
};

krb5_principal Principal::operator() () {
    return _principal;
};

Context& Principal::getCtx() const {
    return ctx;
}

const char* Principal::getRealm() const {
    return krb5_princ_realm(null,_principal)->data;
}

int Principal::getDataLength() const {
    if (_principal)
        return krb5_princ_size(null,_principal);
    else
        return 0;
}
const char* Principal::getData (const int i) const {
    if ( (i >= 0) && (i < getDataLength() ) )
        return (char *) krb5_princ_component(null,_principal,i);
    else
        return NULL;
}

const string Principal::getName() const {
    string res;
    if (_principal) {
        int len = getDataLength();
        for (int i = 0; i < len; i++) {
            res = res.append (getData (i) );
            if (i < (len - 1) )
                res.append ("/");
        }
        res.append ("@");
        res.append (getRealm() );
    }
    return string (res);
}


Credentials::Credentials (Context& _ctx) : Base(), ctx (_ctx), _creds (NULL), server (NULL) {
    _creds = new krb5_creds();
    memset (_creds, 0, sizeof (*_creds) );
};

Credentials::Credentials (Context& _ctx, krb5_creds* c) : Base(), ctx (_ctx), _creds (c), server (NULL) {
    calcServer();
};


Credentials::Credentials (const Credentials& o) : Base(), ctx (o.getCtx() ), _creds (NULL), server (NULL) {
    //_creds=new krb5_creds();
    *this = o;
}

Credentials::~Credentials() {
    if (server != NULL)
        delete server;
    if (_creds != NULL) {
        krb5_free_creds (ctx(), _creds);
    };
};

void Credentials::clear() {
    if (server != NULL) {
        delete server;
        server = NULL;
    }
    if (_creds != NULL) {
        krb5_free_creds (ctx(), _creds);
        _creds = NULL;
    }
    _creds = new krb5_creds();
    memset (_creds, 0, sizeof (*_creds) );

}

krb5_creds * Credentials::operator() () {
    return _creds;
};

const krb5_creds * Credentials::operator() () const {
    return _creds;
};

const Principal* Credentials::getServer() const {
    return server;
}

void Credentials::calcServer() {
    if (server != NULL) {
        delete server;
        server = NULL;
    }
    if ( (_creds != NULL) && (_creds->server != NULL) )
        server = new Principal (ctx, _creds->server, false);
};

long Credentials::getStartTime() const {
    return _creds->times.starttime;
}
long Credentials::getEndTime() const {
    return _creds->times.endtime;
}
long Credentials::getRenewTill() const {
    return _creds->times.renew_till;
}

Credentials& Credentials::operator= (const Credentials& o) {
    clear();
    if (o()->client != NULL) {
        if (_creds != NULL) {
            krb5_free_creds (ctx(), _creds);
            _creds = NULL;
        }
        kerror = krb5_copy_creds (ctx(), o(), &_creds);
    }
    calcServer();
    return *this;
};

Context& Credentials::getCtx() const {
    return ctx;
}

TicketManager::TicketManager (bool _doAklog) : Base(), doAklog (_doAklog), cc (ctx) {
    kerror = ctx.error();
    if (!kerror)
        kerror = cc.error();
}

TicketManager::~TicketManager() {
}

string TicketManager::readPass (int length) {
    char buf[length+1];
    int fd = dup (0);
    FILE* fp = fdopen (fd, "r");
    setvbuf (fp, NULL, _IONBF, 0);
    fd = fileno (fp);
    struct termios tparm;
    struct termios saveparm;
    tcgetattr (fd, &tparm);
    saveparm = tparm;
    tparm.c_lflag &= ~ (ECHO | ECHONL);
    tcsetattr (fd, TCSANOW, &tparm);
    char* ret = fgets (&buf[0], length, fp);
    tcsetattr (0, TCSANOW, &saveparm);
    if (ret == &buf[0]) {
        // remove newline
        int len = strlen (&buf[0]);
        if ( (len > 0) && (buf[len-1] == '\n') )
            buf[len-1] = 0;
        return string (buf);
    } else
        return string ("");
}

bool TicketManager::initKerberos() {
    kerror = 0;
    ctx.reinit();
    if ( (kerror = ctx.error() ) ) {
        kDebug() << "Kerberos returned on context reinit" << kerror;
        return FALSE;
    }
    cc = Krb5::CredentialCache (ctx);
    if ( (kerror = cc.error() ) ) {
        kDebug() << "Kerberos returned on ccache init" << kerror;
        return FALSE;
    }
    kDebug() << "Set cc to " << cc.name().c_str();
    const Krb5::Principal* pme = cc.getPrincipal();
    if (pme) {
        const Krb5::Principal& me = *pme;
        if ( (kerror = cc.error() ) )
        {
            kDebug() << "Kerberos returned on principal retrieval" << kerror;
            return FALSE;
        }
        kDebug() << "Principal is " << me.getName().c_str();
    }
    return TRUE;
}

bool TicketManager::renewTicket() {
    // Can't renew tickets if we don't have any to renew
    if (!authenticated)
        return FALSE;

    const Krb5::Principal* pme = cc.getPrincipal();

    if (!pme) {
        kDebug() << "no principal found";
        kerror = cc.error();
        return FALSE;
    }
    {
        const Krb5::Principal& me = *pme;
        //kDebug() << "renewing tickets for " << me.getData(0) << "@" << me.getRealm();
        kDebug() << "renewing tickets for " << me.getName().c_str();

        Krb5::Credentials my_creds (cc.renew_creds() );
        if ( (kerror = cc.error() ) )
        {
            kDebug() << "Kerberos returned " << kerror <<
            " while renewing creds";
            return FALSE;
        }

        cc.store (my_creds);
        if ( (kerror = cc.error() ) )
        {
            kDebug() << "Kerberos returned " << kerror <<
            " while storing credentials";
            return FALSE;
        }
        kDebug() << "Successfully renewed tickets";

    }
    return TRUE;

}

bool TicketManager::hasTicket() {
    int noTix = 1;

    qint32 now;


    kDebug() << "Called hasCurrentTickets()";

    /* if kerberos is not currently happy, try reinitializing.  The user may
       have obtained new tickets since we last initialized.
    */
    if (kerror)
    {

        kDebug() << "hasCurrentTickets(): kerror = " << kerror;
        kDebug() << "Trying to reinitialize kerberos...";
        initKerberos();
        authenticated = 0;

    }

    now = time (0);
    Krb5::Principal* pme = cc.getPrincipal();
    if ( (kerror = cc.error() ) || (!pme) )
    {
        kDebug() << "While retrieving principal name, Kerberos returned " << kerror;
        authenticated = 0;
        return FALSE;
    }
    Krb5::Principal& me = *pme;
    kDebug() << "Princ is " << me.getName().c_str();
    Krb5::CredentialCacheIterator iter = cc.iterator();
    if ( (kerror = iter.error() ) )
    {
        kDebug() << "While beginning CC iterations, kerberos returned " << kerror;
        authenticated = 0;
        return FALSE;
    }
    kDebug() << "Start iter";
    while (! (iter.error() ) ) {
        const Krb5::Credentials& creds = *iter;
        const Principal* pserver = NULL;
        if (!iter.error() )
            pserver = creds.getServer();
        else {
            kDebug() << "Error: Iterator returned no creds";
            break;
        }
        if (pserver) {
            const Principal& server = *pserver;
            if (noTix && server.getDataLength() == 2 &&
                    string (server.getRealm() ) == me.getRealm() &&
                    string (server.getData (0) ) == "krbtgt" &&
                    string (server.getData (1) ) == me.getRealm() &&
                    creds.getEndTime() > now) {
                noTix = 0;
                tktExpirationTime = creds.getEndTime();
                tktRenewableExpirationTime = creds.getRenewTill();
                break;;
            }
        }
        iter++;
    }
    noTix == 0 ? authenticated = 1 : authenticated = 0;
    kDebug() << "hasCurrentTickets set authenticated=" << authenticated;
    return authenticated;
};


bool TicketManager::getCredentials (const char* pass) {
    krb5_get_init_creds_opt options;
    krb5_get_init_creds_opt_init (&options);
    Credentials my_creds (ctx);
    Principal *osMe (NULL);
    Krb5::Principal* pme = cc.getPrincipal();
    if (!pme || cc.error() ) {
        initKerberos();
        osMe = osPrincipal();
        if (! (pme = osMe) ) {
            kDebug() << "Could not get Principal from os";
            kerror = -1;
            return FALSE;
        };
    }
    Krb5::Principal& me = *pme;
    char* ppass = (char*)pass;

    if (strlen(pass) == 0) return false;

    kerror =
        krb5_get_init_creds_password (ctx(), my_creds(), me(),
                                      ppass, 0, 0,
                                      0,
                                      0,
                                      &options);
    if (kerror) {
        kDebug() << "Error on passGetCreds " << kerror;
        return FALSE;
    }
    cc.store (my_creds);
    if ( (kerror = cc.error() ) ) {
        kDebug() << "Error on storing creds in passGetCreds " << kerror;
        return FALSE;
    }
    return TRUE;
};

bool TicketManager::runAklog() {
    if (!doAklog) return TRUE;
    if (authenticated) {
        kDebug() << "Calling aklog";
        int ret = system ("aklog");
        if (ret == -1)
            kDebug() << "Unable to run aklog";
        if (ret > 0)
            kDebug() << "aklog failed with" << ret;
        if ( ret != 0 )
            return FALSE;
        else
            return TRUE;
    } else
        return FALSE;

};

bool TicketManager::runUnlog() {
    if (!doAklog) return TRUE;
    if (authenticated) {
        kDebug() << "Calling unlog";
        int ret = system ("unlog");
        if (ret == -1)
            kDebug() << "Unable to run unlog";
        if (ret > 0)
            kDebug() << "unlog failed with" << ret;
        if ( ret != 0 )
            return FALSE;
        else
            return TRUE;
    } else
        return FALSE;

};

Principal* TicketManager::osPrincipal() {
    Principal* ret = NULL;
    struct passwd *pw;
    if ( (pw = getpwuid ( (int) getuid() ) ) ) {
        krb5_principal pOsMe;
        kerror = krb5_parse_name (ctx(), pw->pw_name, &pOsMe);
        ret = new Principal (ctx, pOsMe, TRUE);
        krb5_free_principal (ctx(), pOsMe);
        if (kerror) {
            kDebug() << "Parsing user name failed";
            delete ret;
            return NULL;
        }
        cc.setPrincipal (*ret);
        return ret;
    } else {
        kDebug() << "Could not get user name";
        return NULL;
    }
}

bool TicketManager::destroyTicket() {
    cc.destroy();
    if (cc.error() )
        return FALSE;
    else
        return runUnlog();
}

}
