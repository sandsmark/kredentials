/*
 * Copyright 2006 by Martin Ginkel, MPI-Magdeburg <mginkel@mpi-magdeburg.mpg.de>
 * and Copyright 2004 by the Massachusetts Institute of Technology.
 * All Rights Reserved.
 *
 * Permission to use, copy, modify, and distribute this software and
 * its documentation for any purpose and without fee is hereby
 * granted, provided that the above copyright notice appear in all
 * copies and that both that copyright notice and this permission
 * notice appear in supporting documentation, and that the name of
 * M.I.T. not be used in advertising or publicity pertaining to
 * distribution of the software without specific, written prior
 * permission.  Furthermore if you modify this software you must label
 * your software as modified software and not distribute it in such a
 * fashion that it might be confused with the original
 * M.I.T. software.  M.I.T. makes no representations about the
 * suitability of this software for any purpose.  It is provided "as
 * is" without express or implied warranty.
 *
 * $Id: krb5_wrap.h 119 2007-03-13 14:38:00Z noahm $
 */

#ifndef KRB5_WRAP_H
#define KRB5_WRAP_H
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <krb5.h>
#include <string>
//#undef DEBUG
/**
   Namespace for kerberos wrapper objects
*/
namespace Krb5 {
/**
   Baseclass for error handling
 */
class Base {
protected:
    /**
       errorcode of last krb5_xxx operation
     */
    krb5_error_code kerror;
public:
    /**
       initialize error to 0
     */
    Base() : kerror (0) {};
    /**
       get the error of last operation
     */
    krb5_error_code error() const;
};

/**
   wrapper for krb5_context
 */
class Context: public virtual Base {
private:
    krb5_context _ctx;

public:
    Context();
    virtual ~Context();
    krb5_context operator() () const;
    void reinit();
};

class CredentialCache;
class Principal;
/**
   wrapper for krb5_creds
 */
class Credentials: public virtual Base {
private:
    Context& ctx;
    krb5_creds * _creds;
    Principal * server;
public:
    Credentials (Context& _ctx);
    // Take given creds and own them!
    Credentials (Context& _ctx, krb5_creds* o);
    // Dont use, bug? in krb5_copy_creds
    Credentials (const Credentials& o);

    virtual ~Credentials();

    const krb5_creds * operator() () const;
    Context& getCtx() const;
    const Principal* getServer() const;
    long getStartTime() const;
    long getEndTime() const;
    long getRenewTill() const;

    void clear();
    krb5_creds * operator() () ;
    // Dont use, bug? in krb5_copy_creds
    Credentials& operator= (const Credentials& o);
    void calcServer();
};

class Principal: public virtual Base {
private:
    bool free;
    Context& ctx;
    krb5_principal  _principal;

public:
    Principal (Context& _ctx, CredentialCache& _cc);
    Principal (Principal& o);
    Principal (Context& _ctx, krb5_principal p, bool copy = false);
    virtual ~Principal();
    krb5_principal operator() ();

    Context& getCtx() const;
    const char* getRealm() const;
    int getDataLength() const ;
    const char* getData (const int i) const;
    const std::string getName() const;
};

class CredentialCacheIterator: public virtual Base {
private:
    CredentialCache& cc;
    krb5_cc_cursor cur;
    Credentials * pcreds;
    void next();
public:
    CredentialCacheIterator (CredentialCache& _cc);
    virtual ~CredentialCacheIterator();
    const Credentials& operator*();
    CredentialCacheIterator& operator++ (int dummy);

};

class CredentialCache: public virtual Base {
private:
    krb5_ccache   _cc;
    Context&      ctx;
    std::string _name;
    Principal*  _principal;
    Credentials*      _creds;
    
public:
    CredentialCache (Context& _ctx);
    virtual ~CredentialCache();
    krb5_ccache operator() () const;
    Context& getCtx() const;
    CredentialCacheIterator iterator();
    const std::string& name();
    void store (Credentials& creds);
    void setPrincipal (Principal& me);
    Principal* getPrincipal();
    Credentials& renew_creds();
    CredentialCache& operator= (const CredentialCache& o);
    void destroy();
};

class TicketManager: public virtual Base {

protected:

    bool doAklog;
    int kerror;
    int authenticated;
    time_t tktExpirationTime;
    time_t tktRenewableExpirationTime;
    Context ctx;
    CredentialCache cc;


public:
    TicketManager (bool _doAklog = true);
    virtual ~TicketManager();
    std::string readPass (int length = 1024);
    /**
       return a fresh principal from the user-info of the OS
     */
    virtual Principal* osPrincipal();
    virtual bool renewTicket();
    virtual bool hasTicket();
    virtual bool initKerberos();
    virtual bool getCredentials (const char* pass);
    virtual bool runAklog();
    virtual bool runUnlog();
    virtual bool destroyTicket();
};



}

#endif
