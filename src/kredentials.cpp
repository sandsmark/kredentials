/*
 * Copyright 2004 by the Massachusetts Institute of Technology.
 * All Rights Reserved.
 *
 * Permission to use, copy, modify, and distribute this software and
 * its documentation for any purpose and without fee is hereby
 * granted, provided that the above copyright notice appear in all
 * copies and that both that copyright notice and this permission
 * notice appear in supporting documentation, and that the name of
 * M.I.T. not be used in advertising or publicity pertaining to
 * distribution of the software without specific, written prior
 * permission.  Furthermore if you modify this software you must label
 * your software as modified software and not distribute it in such a
 * fashion that it might be confused with the original
 * M.I.T. software.  M.I.T. makes no representations about the
 * suitability of this software for any purpose.  It is provided "as
 * is" without express or implied warranty.
 *
 * $Id: kredentials.cpp 118 2007-03-13 14:29:42Z noahm $
 */

#include "kredentials.h"

#include <QLabel>
#include <QCursor>
#include <QEvent>
#include <QMouseEvent>
#include <QTimerEvent>

#include <KActionCollection>
#include <KApplication>
#include <KMainWindow>
#include <KLocale>
#include <KDebug>
#include <KIconLoader>
#include <KMessageBox>
#include <KMenu>
#include <KPassivePopup>
#include <KPasswordDialog>

#include <time.h>
#include <string.h>
#include <iostream>

#ifndef NDEBUG
#define DEFAULT_RENEWAL_INTERVAL 20
#define DEFAULT_WARNING_INTERVAL 3600
#else
#define DEFAULT_RENEWAL_INTERVAL 3600
#define DEFAULT_WARNING_INTERVAL 86400
#endif /*DEBUG*/

Kredentials::Kredentials (int notify, int aklog)
        : KSystemTrayIcon(), TicketManager (aklog)
{
    // set the shell's ui resource file
    //setXMLFile("kredentialsui.rc");

    kDebug() << "kredentials constructor called";

    m_doNotify = notify;
    doAklog  = aklog;
    m_renewWarningTime = DEFAULT_WARNING_INTERVAL;
    m_secondsToNextRenewal = DEFAULT_RENEWAL_INTERVAL;
    m_renewWarning = false;
    this->setIcon (this->loadIcon ("kredentials") );

    KMenu *menu = new KMenu();
    menu->addAction(SmallIcon("view-refresh"), i18n("&Renew credentials"), this, SLOT(renewTicket()));
    menu->addAction(SmallIcon("document-new"), i18n("&Get new credentials"), this, SLOT(acquireTicket()));
    menu->addAction(SmallIcon("svn_status"), i18n("&Credentials status"), this, SLOT(displayTicket()));
    menu->addAction(SmallIcon("edit-delete"), i18n("&Destroy credentials"), this, SLOT(destroyTicket()));
    menu->addAction(SmallIcon("application-exit"), i18n("Quit"), kapp, SLOT (quit()));
    setContextMenu(menu);

    //initKerberos();
    hasTicket();

    m_timer = startTimer (1000);

    kDebug() << "Using Kerberos KRB5CCNAME of " << cc.name().c_str(); // gah, obfuscated code...
    kDebug() << "kredentials constructor returning";
}

Kredentials::~Kredentials()
{
}



void Kredentials::mousePressEvent (QMouseEvent *e)
{
    if (e->button() == Qt::RightButton)
    {
        contextMenu()->popup (QCursor::pos() );
    }
}

bool Kredentials::destroyTicket() {
   if (!TicketManager::destroyTicket()) {
        KMessageBox::sorry (0, i18n ("Unable to destroy your tickets."), 0, 0);
        return false;
    } else {
        KMessageBox::information (0, i18n ("Your tickets have been destroyed."), 0, 0);
        return true;
    }
}

void Kredentials::acquireTicket() {
    Krb5::Principal *osMe = 0;

    Krb5::Principal* pme = cc.getPrincipal();
    QString myName = i18n ("Please give a password for ");
    if (!pme) {
        osMe = osPrincipal();
        pme = osMe;
    }
    if (pme) {
        myName.append(pme->getName().c_str());
    } else {
        myName.append("unknown user");
    }
    killTimer(m_timer);
    kDebug() << "Getting Pass";

    KPasswordDialog dlg(0);
    dlg.setPrompt(myName);
    dlg.setWindowTitle("Please enter user's password");

    if (dlg.exec() == QDialog::Accepted) {
        kDebug() << "Getting Creds" << endl;
        bool res = getCredentials (dlg.password().toLocal8Bit());
        kDebug() << "Finished Creds" << endl;
        if (!res) {
            KMessageBox::sorry (0, i18n ("Your password was probably wrong"),
                                0, 0);
            return;
        } else {
            hasTicket();
            if (!runAklog()) {
                KMessageBox::sorry (0, i18n ("Unable to run aklog"), 0, 0);
            }
        }
    };
    m_timer = startTimer (1000);
}

bool Kredentials::renewTicket()
{
    time_t now = time (0);
    killTimer(m_timer);

    if (!hasTicket() ) {
        acquireTicket();
    } else if (tktRenewableExpirationTime == 0) {
        acquireTicket();
    }
    else if (tktRenewableExpirationTime < now) {
        KMessageBox::information (0, "Your tickets have outlived their renewable lifetime and can't be renewed.", 0, 0);
        kDebug() << "tktRenewableExpirationTime has passed: ";
        kDebug() << "tktRenewableExpirationTime = " <<
        tktRenewableExpirationTime << ", now = " << now;
        acquireTicket();
    } else if (!renewTicket() ) {
        kDebug() << "renewTickets did not get new tickets";

        if (!hasTicket() ) {
            acquireTicket();
        }
    } else {
        if (m_doNotify) {
            KPassivePopup::message ("Kerberos tickets", "Kerberos tickets have been renewed", KIcon("checkbox").pixmap(25, 25), this, 0);
        }
    }

    // restart the timer here, regardless of whether we currently
    // have tickets now or not.  The user may get tickets before
    // the next timeout, and we need to be able to renew them
    m_secondsToNextRenewal = DEFAULT_RENEWAL_INTERVAL;
    m_timer = startTimer (1000);
    if (authenticated > 0) {
        if (!runAklog()) {
            KMessageBox::sorry (0, "Unable to run aklog", 0, 0);
        }

        kDebug() << "WarnTime: " << m_renewWarningTime << " " <<
        m_doNotify;
        if (m_doNotify && tktRenewableExpirationTime - now < m_renewWarningTime) {
            kDebug() << "Renew=" << m_renewWarning;
            if (m_renewWarning) {
                m_renewWarning = false;
                kDebug() << "RESET: Renew=" << m_renewWarning;

                QString msgString =
                    QString ("Kerberos tickets will permanently expire on ")
                    +  QString (ctime (&tktRenewableExpirationTime) ) +
                    QString (" You may want to renew them now.");
                KMessageBox::information (0, msgString, 0, 0);
            }
        } else {
            m_renewWarning = true;
            kDebug() << "RESET: Renew=" << m_renewWarning;
        }
    }
    return false;
}



void Kredentials::timerEvent (QTimerEvent*)
{

    kDebug() << "timerEvent triggered, secondsToNextRenewal == "
    << m_secondsToNextRenewal;

    m_secondsToNextRenewal--;
    if (m_secondsToNextRenewal < 0)
    {
        renewTicket();
    }
    return;
}

void Kredentials::displayTicket()
{
    hasTicket();
    QString msgString;

    if (!authenticated)
    {
        KMessageBox::information (0,
                                  "You do not have any valid tickets.",
                                  "Kerberos", 0, 0);
    }
    else
    {
        const Krb5::Principal* pme = cc.getPrincipal();
        if (pme) {
            const Krb5::Principal& me = *pme;
            if (me.getDataLength() ) {
                msgString = QString ("Your tickets as ")
                            + QString (me.getName().c_str() ) + QString (" ");
            } else {
                msgString = QString ("Your tickets ");
            }
        }
        msgString += QString ("are\n Valid until ") +
                     QString (ctime (&tktExpirationTime) );
        if (tktRenewableExpirationTime > time (0) )
        {
            msgString += QString ("\nRenewable until ") +
                         QString (ctime (&tktRenewableExpirationTime) );
        }
        else
        {
            msgString += QString ("\nTickets are not renewable");
        }
        KMessageBox::information (0, msgString, "Kerberos", 0, 0);
    }
    return;
}

void Kredentials::setDoNotify (int state)
{
    m_doNotify = state;
}

void Kredentials::setDoAklog (int state)
{
    doAklog = state;
}

#include "kredentials.moc"
