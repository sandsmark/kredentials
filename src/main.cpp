/*
 * Copyright 2004 by the Massachusetts Institute of Technology.
 * Copyright 2009 Martin T. Sandsmark <sandsmark@samfundet.no>
 * All Rights Reserved.
 *
 * Permission to use, copy, modify, and distribute this software and
 * its documentation for any purpose and without fee is hereby
 * granted, provided that the above copyright notice appear in all
 * copies and that both that copyright notice and this permission
 * notice appear in supporting documentation, and that the name of
 * M.I.T. not be used in advertising or publicity pertaining to
 * distribution of the software without specific, written prior
 * permission.  Furthermore if you modify this software you must label
 * your software as modified software and not distribute it in such a
 * fashion that it might be confused with the original
 * M.I.T. software.  M.I.T. makes no representations about the
 * suitability of this software for any purpose.  It is provided "as
 * is" without express or implied warranty.
 *
 * $Id: main.cpp 108 2007-03-12 18:30:39Z noahm $
 */

#include "kredentials.h"
#include <kuniqueapplication.h>
#include <kaboutdata.h>
#include <kcmdlineargs.h>
#include <KLocale>
#include <QDebug>

static const char description[] =
    I18N_NOOP ("Monitor and update authentication tokens");

static const char version[] = "1.9.0";

int main (int argc, char *argv[])
{
    KAboutData about ("kredentials",
                      0,
                      ki18n ("Kredentials"),
                      version,
                      ki18n (description),
                      KAboutData::License_GPL_V3,
                      ki18n ("Copyright 2004 Noah Meyerhans\nCopyright 2009 Martin Sandsmark"),
                      KLocalizedString(),
                      "http://mts-productions.com/");

    KCmdLineArgs::init (argc, argv, &about);

    KCmdLineOptions options;
    options.add ("+[inform]", ki18n ("Informs the user when credentials are renewed.") );
    options.add ("+[noaklog]", ki18n ("Don't run aklog to update AFS tokens when renewing Kerberos tickets") );
    KCmdLineArgs::addCmdLineOptions (options);
    KCmdLineArgs *args = KCmdLineArgs::parsedArgs();

    if (!KUniqueApplication::start() ) {
        qWarning() << "Kredentials is already running!\n";
        exit (0);
    }


    KUniqueApplication app;
    app.disableSessionManagement();
    Kredentials *mainWin = 0;

    mainWin = new Kredentials();
    if (args->isSet ("+[inform]") )
    {
        mainWin->setDoNotify (true);
    }
    if (args->isSet ("+[noaklog]") )
    {
        mainWin->setDoAklog (false);
    }

    mainWin->show();

    //args->clear();

    // mainWin has WDestructiveClose flag by default, so it will delete itself.
    return app.exec();
}

