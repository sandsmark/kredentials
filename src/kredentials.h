/*
 * Copyright 2004 by the Massachusetts Institute of Technology.
 * All Rights Reserved.
 *
 * Permission to use, copy, modify, and distribute this software and
 * its documentation for any purpose and without fee is hereby
 * granted, provided that the above copyright notice appear in all
 * copies and that both that copyright notice and this permission
 * notice appear in supporting documentation, and that the name of
 * M.I.T. not be used in advertising or publicity pertaining to
 * distribution of the software without specific, written prior
 * permission.  Furthermore if you modify this software you must label
 * your software as modified software and not distribute it in such a
 * fashion that it might be confused with the original
 * M.I.T. software.  M.I.T. makes no representations about the
 * suitability of this software for any purpose.  It is provided "as
 * is" without express or implied warranty.
 *
 * $Id: kredentials.h 108 2007-03-12 18:30:39Z noahm $
 */

#ifndef _KREDENTIALS_H_
#define _KREDENTIALS_H_

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <KSystemTrayIcon>
#include <qtimer.h>
#include <qevent.h>
#include <qpixmap.h>
//Added by qt3to4:
#include <QMouseEvent>
#include <QTimerEvent>
#include <kuser.h>
#include <kaction.h>
#include <kdialog.h>
#include <QMenu>

#include <time.h>
#include "krb5_wrap.h"

/**
 * @short Application Main Window
 * @author Noah Meyerhans <noahm@csail.mit.edu>
 * @author Martin Sandsmark <sandsmark@samfundet.no>
 */

class Kredentials : public KSystemTrayIcon, public Krb5::TicketManager
{
    Q_OBJECT
public:
    /**
     * Default Constructor
     */
    //kredentials();
    Kredentials ( int doNotify = 0, int doAklog = 1 );

    /**
     * Default Destructor
     */
    virtual ~Kredentials();
    void setDoNotify (int);
    void setDoAklog (int);

protected slots:
    bool renewTicket();
    void displayTicket();
    void acquireTicket();
    bool destroyTicket();

protected:
    void mousePressEvent (QMouseEvent *);
    void timerEvent (QTimerEvent *);

private:
    int m_secondsToNextRenewal;
    bool m_renewWarning;
    int m_renewWarningTime;

    int m_doNotify;

    int m_timer;

};

#endif // _KREDENTIALS_H_
